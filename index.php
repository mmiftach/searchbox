 <?php  
	function http_request($url){
    // persiapkan curl
    $ch = curl_init(); 

    // set url 
    curl_setopt($ch, CURLOPT_URL, $url);
    

    // return the transfer as a string 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

    // $output contains the output string 
    $output = curl_exec($ch); 

    // tutup curl 
    curl_close($ch);      

    // mengembalikan hasil curl
    return $output;

}

	$profile = http_request("http://localhost:8080/all/?key=123");

	// ubah string JSON menjadi array
	$profile = json_decode($profile, TRUE);
	//echo "<pre>";
	//print_r($profile);
	//echo "</pre>";
 
 
 ?>  
 <!DOCTYPE html>  
 <html>  
  <head>  
           <title>TG Checking</title>  
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>  
           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
		   <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" /> 
           <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
		   <script  src = "https://code.jquery.com/jquery-3.3.1.js"></script>
			<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
      </head> 
	  <body>
	  <br /><br />  
           <div class="container" style="width:900px;">  
                <h2 align="center">TG Checking</h2>  
                <h3 align="center">Dashboard Tunggakan Telkom Group</h3>                 
                <br /><br />  
      <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th width="5%">ID</th>  
                               <th width="25%">Company Name</th>  
                               <th width="35%">Due Date</th>  
                               <th width="15%">Invoice Source</th>  
                               <th width="20%">Value</th>  
                               <th width="5%">Currency</th> 
            </tr>
        </thead>
        <tbody>
			<?php 
			$total = 0;
			foreach ($profile['data'] as $rows) :?>	
			<tr>  
				<td><?php echo $rows['id'];?></td>  
				<td><?php echo $rows['CompanyName'];?></td>  
				<td><?php echo $rows['DueDate'];?></td>  
                <td><?php echo $rows['InvoiceSource'];?></td>  
                <td><?php echo $rows['Value'];?></td>  
                <td><?php echo $rows['Currency'];?></td>  
            </tr> 
			<?php endforeach;?>
           
        </tbody>
        <tfoot>
            
        </tfoot>
    </table>
	</body>
 </html>  
 <script>  
 $(document).ready(function() {
    $('#example').DataTable( {
        "pagingType": "full_numbers"
    } );
} );
 </script>  